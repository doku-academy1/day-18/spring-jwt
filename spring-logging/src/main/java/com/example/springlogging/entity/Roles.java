package com.example.springlogging.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "roles")
//@NoArgsConstructor

public class Roles implements Serializable {
       @Id
       @GeneratedValue(strategy = GenerationType.IDENTITY)
       private Long roleId;

       @Enumerated(EnumType.STRING)
       @Column
       private ERole name;

       public Roles() {}

       public Roles(ERole name) {
              this.name=name;
       }

       public Long getRoleId() {
              return roleId;
       }

       public void setRoleId(Long roleId) {
              this.roleId = roleId;
       }

       public ERole getName() {
              return name;
       }

       public void setName(ERole name) {
              this.name = name;
       }
}
