package com.example.springlogging;

import com.example.springlogging.security.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
//@EnableConfigurationProperties()
public class SpringLoggingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLoggingApplication.class, args);
	}

}
