package com.example.springlogging.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@Configuration
public class BeanRegister {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
