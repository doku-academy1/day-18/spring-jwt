package com.example.springlogging.repository;

import com.example.springlogging.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
        Optional<User> findByUsername(String username);
        Optional<User> findByEmail(String name);

        Boolean existsByUsername(String username);

        Boolean existsByEmail(String email);
}
